class Form {
	constructor(submitRes) {
		this.textInput = document.getElementById('form-text-input');
		this.submit = document.getElementById('form-submit');
		this.res = submitRes;
		this.submit.addEventListener('click', () => {
			this.displayResponse();
		});
		this.textInput.addEventListener('keyup', e => {
			if (e.keyCode === 13) {
				this.submit.click();
			}
		});
	}

	logForm() {
		console.log(this);
	}

	displayResponse() {
		alert(this.res);
	}
}

let form = new Form('Form Submitted!');

//test 2
