(function() {
	console.log(atts);
	var host = window.location.hostname;
	let viewer = new PTGuiViewer();
	// viewer.setSwfUrl('../PTGuiViewer.swf');
	viewer.setSwfUrl(
		`${host}/wp-content/plugins/panorama-viewer/PTGuiViewer.swf`
	);
	viewer.preferHtmlViewer();
	// viewer.preferFlashViewer();
	viewer.setVars({
		pano: atts.src,
		format: '14faces',
		pan: 0,
		minpan: -180,
		maxpan: 180,
		tilt: 0,
		mintilt: -74.23880597014926,
		maxtilt: 70.23880597014926,
		fov: 120,
		minfov: 10,
		maxfov: 120,
		autorotatespeed: 0,
		autorotatedelay: 0,
		maxiosdimension: 567,
		showfullscreenbutton_flash: 1,
		showfullscreenbutton_html: 1,
		enablegyroscope: 1
	});
	if (atts.lightbox === 'true') {
		let lightbox = document.getElementById(`${atts.name}-lightbox`);
		lightbox.addEventListener('click', () => {
			viewer.showInLightbox();
			return false;
		});
	} else {
		viewer.embed(atts.name);
	}
})();
