<?php
/**
 *Plugin Name: Panorama Viewer
 *Description: Shortcode implementation of PTGui Panorama Viewer
 *Author: Greg Lorenzen
 *Version: 1.1.3
**/
  
class Player {

    public function __construct() {
        add_shortcode('pano_player', array($this, 'shortcode'));
    }

    public function enqueue_player_scripts($atts) {
        wp_enqueue_script('pt-viewer-js', plugin_dir_url( __FILE__ ) . 'js/PTGuiViewer.js', '', '', true);
        wp_enqueue_script('viewer-js' . $atts['name'], plugin_dir_url( __FILE__ ) . 'js/viewer.js', '', '1.0', true);
        wp_enqueue_style('panorama-viewer-styles', plugin_dir_url(__FILE__) . 'css/style.css');
        wp_localize_script('viewer-js'. $atts['name'], 'atts', $atts);
    }

    public function shortcode($atts) {

        // 'img' is the id of any one of the images for a specific panorama
        // 'name' is the user create name for the viewer (must be unique)

        $a = shortcode_atts(array(
            'img' => '',
            'name' => 'mypanoviewer',
            'lightbox' => 'false'
        ), $atts, 'panorama-viewer');

        // Display only if an img is provided

        if(!$a['img']) {
            return;
        }

        $src = wp_get_attachment_image_src($a['img'], 'full', false);

        // parse URL
        $parsed_url = parse_url($src[0], PHP_URL_PATH);

        // split the string at the image number
        $pathComponents = explode('_', $parsed_url);

        // set the string to be removed equal to the image number
        $search = $pathComponents[1];
        $trimmed = str_replace($search, '', $src);
        $a += array(
            'src' => $trimmed[0]
        );

        $this->enqueue_player_scripts($a);

        if($a['lightbox'] === 'true' || $a['lightbox'] === 'yes' || $a['lightbox'] === '1') {
            return "<span id=" . $a['name'] . '-lightbox' . " class='lightbox-img'><img src=" . $src[0] .  " class='pano-thumbnail'></a>";
        }
        else {
            return "<div id=" . $a['name'] . " class='pano-viewer'></div>";
        }
    }
}

$player = new Player();